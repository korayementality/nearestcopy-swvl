const readline = require('readline');
const fs = require('fs');

const { shortestPath, sortNumbers } = require('./shortestPath');

const input = fs.createReadStream('./input.txt');
const rl = readline.createInterface({ input, output: process.stdout, terminal: false });
// 1: nodes/edge -> 2: connections -> 3: ids -> 4: id required
let inputMode = 1;
// Graph nodes
let nodes = [];
// Node IDs we're going to assign
let ids = [];
// We use this to iterate over edge lines in input file
let edgesIterator = 0;
let totalNodes = 0;
let totalEdges = 0;
// The ID we're looking for
let targetId;

/**
 * Attempts to read line by line and setup a graph based on input.txt
 */
rl.on('line', (line) => {
  try {
    /**
   * Read the First Line
   */
    if (inputMode === 1) {
      const firstLine = line.split(' ');
      if (firstLine.length !== 2) throw Error('Invalid Node/Edge format on the first line');
      [totalNodes, totalEdges] = firstLine;
      inputMode++;
    /**
     * Read M number of lines for our edges
     */
    } else if (inputMode === 2) {
      if (edgesIterator < totalEdges) {
        /**
       * Constructs an array of nodes with corresponding neighbours based on provided edges
       * If a node already exists it appends the new neighbour, else it constructs a new node with new neighbour
       * Then attempts to do the same procedure for the neighbour itself
       *
       * Also to simplify, edges are always sorted from lower to higher. 4-->2 is changed to 2-->4
       * Finally, two way connections are made, so if there's 2-->4 connection for Node 2,
       * there's also a 4-->2 connection for Node 4
       */
        const edgeLine = sortNumbers(line.split(' '));
        let targetNode = nodes.find(node => node && node.nodeIndex === edgeLine[0]);
        if (targetNode === undefined) {
          targetNode = { nodeIndex: edgeLine[0], visited: false };
          targetNode.neigbours = [].concat(edgeLine[1]);
          targetNode.neigbours = sortNumbers(targetNode.neigbours);
          nodes.push(targetNode);
        } else {
          targetNode.neigbours.push(edgeLine[1]);
          targetNode.neigbours = sortNumbers(targetNode.neigbours);
        }
        targetNode = nodes.find(node => node && node.nodeIndex === edgeLine[1]);
        if (targetNode === undefined) {
          targetNode = { nodeIndex: edgeLine[1], visited: false };
          targetNode.neigbours = [].concat(edgeLine[0]);
          targetNode.neigbours = sortNumbers(targetNode.neigbours);
          nodes.push(targetNode);
        } else {
          targetNode.neigbours.push(edgeLine[0]);
          targetNode.neigbours = sortNumbers(targetNode.neigbours);
        }
        edgesIterator++;
        if (edgesIterator >= totalEdges) {
          nodes = nodes.sort((a, b) => ((a.nodeIndex > b.nodeIndex) ? 1 : ((b.nodeIndex > a.nodeIndex) ? -1 : 0)));
          inputMode++;
        }
      }


    /**
     * Read N number of node IDs
     */
    } else if (inputMode === 3) {
      ids = line.split(' ');
      if (ids.length !== parseInt(totalNodes)) throw Error('Insufficient number of IDs provided');
      nodes = nodes.map((node, i) => {
        node.id = ids[i];
        return node;
      });
      inputMode++;
    } else if (inputMode === 4) {
      targetId = line;
      rl.emit('doneReadingFile');
    }
  } catch (e) {
    throw Error(e);
  }
});

// Reading input complete, begin algorithm
rl.on('doneReadingFile', () => {
  shortestPath(targetId, nodes);
});
