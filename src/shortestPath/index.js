/**
 * Used when reading input line by line to sort nodes and edges ascendingly
 */
const sortNumbers = arr => arr.sort((a, b) => a - b);


const BFS = (id, nodes, startNode) => {
  let path = 0;
  let foundNode = false;
  /**
   * Set all nodes to be unvisited
   */
  nodes = nodes.map((node) => {
    node.visited = false;
    return node;
  });

  // UNCOMMENT TO DEBUG
  // console.log(nodes);


  // Start queuing
  let queue = [startNode];
  while (queue.length !== 0) {
    // UNCOMMENT TO DEBUG
    // console.log('QUEUE', queue);

    // unqueue head node
    const current = queue.shift() - 1;

    // Break if we found a node with our given ID
    if (nodes[current].id === id && startNode !== nodes[current].nodeIndex) {
      path += 1;
      foundNode = true;
      break;
    }
    // Do not go on loop if this node was visited before
    if (nodes[current].visited) {
      continue;
    }

    /**
     * nextInQueue are nodes that are about to be pushed into our queue
     * They must have not been visited before
     * And they must not be already in our queue
     */
    const nextInQueue = nodes[current].neigbours.filter(neighbourIndex => !nodes[neighbourIndex - 1].visited && (queue.find(element => element === neighbourIndex) === undefined));
    if (nextInQueue.length === 0) {
      path -= 1;
      if (path < 0) path = 0;
      nodes[current].visited = true;


      // UNCOMMENT TO DEBUG
      // console.log(path);

      continue;
    } else {
      path += 1;

      // UNCOMMENT TO DEBUG
      // console.log(path);
      nodes[current].visited = true;
      queue = queue.concat(nextInQueue);
      continue;
      // UNCOMMENT TO DEBUG
      // console.log(path);
    }
  }


  // UNCOMMENT TO DEBUG
  // console.log('final result:', path);

  if (!foundNode) {
    return -1;
  }
  return path;
};


const shortestPath = (id, nodes) => {
  /**
   * nodesOfInterest are the nodes with our given ID
   */
  const nodesOfInterest = nodes.filter(node => node.id === id);
  let path = Infinity;
  /**
   * Attempt to find directly neighboured nodes of interest
   */
  if (nodesOfInterest.some(node => node.neigbours.some(neighbour => nodes[neighbour - 1].id === node.id))) {
    path = 1;
    return path;
  }
  /**
   * Iterate over all nodes of interest
   * to find the shortest possible path
   * for each combination of nodes of the same ID
   */
  for (let i = 0; i < nodesOfInterest.length; i++) {
    const result = BFS(id, nodes, nodesOfInterest[i].nodeIndex);
    if (result !== -1) {
      path = Math.min(path, result);
    }
  }

  // UNCOMMENT TO DEBUG
  // console.log('final result from BFS:', path);


  return path;
};
module.exports = {
  sortNumbers,
  shortestPath,
};
